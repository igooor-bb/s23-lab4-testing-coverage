package com.hw.db.DAO;
import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.validation.constraints.NotNull;
import java.util.List;

class ForumDAOTests {
    JdbcTemplate dbMock;
    ForumDAO forumMock;

    static final String slug = "slug";
    static final String nickname = "some";

    @BeforeEach
    void setUp() {
        this.dbMock = Mockito.mock(JdbcTemplate.class);
        this.forumMock = new ForumDAO(this.dbMock);
    }

    @NotNull
    static List<Arguments> branches() {
        return List.of(
                Arguments.of(
                        slug, null, null, false,
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"
                ),
                Arguments.of(
                        slug, null, null, true,
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc;"
                ),
                Arguments.of(
                        slug, 3, null, false,
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;"
                ),
                Arguments.of(
                        slug, 3, null, true,
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;"
                ),
                Arguments.of(
                        slug, null, nickname, false,
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"
                ),
                Arguments.of(
                        slug, null, nickname, true,
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"
                ),
                Arguments.of(
                        slug, 3, nickname, true,
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"
                )
        );
    }

    @ParameterizedTest
    @MethodSource("branches")
    void testUserList(String slug, Integer limit, String since, Boolean isDescendingOrder, String expectedQuery) {
        ForumDAO.UserList(slug, limit, since, isDescendingOrder);
        Mockito.verify(this.dbMock).query(Mockito.eq(expectedQuery), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }
}