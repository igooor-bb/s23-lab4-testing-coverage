package com.hw.db.DAO;

import com.hw.db.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.sql.Timestamp;

import static org.mockito.Mockito.*;

class PostDAOTests {
    JdbcTemplate dbMock;
    PostDAO postDaoMock;
    Post post;

    static final Integer postId = 1;
    static final String author = "author";
    static final String message = "message";
    static final Timestamp timestamp = new Timestamp(1);

    @BeforeEach
    void setUp() {
        this.dbMock = mock(JdbcTemplate.class);
        this.postDaoMock = new PostDAO(this.dbMock);
        this.post = new Post();
        this.post.setAuthor("");
        this.post.setMessage("");
        this.post.setCreated(new Timestamp(0));
    }

    @NotNull
    static List<Arguments> arguments() {
        return List.of(
                Arguments.of(
                        postId, author, null, null,
                        "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        postId, author, message, null,
                        "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        postId, author, null, timestamp,
                        "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        postId, author, message, timestamp,
                        "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        postId, null, message, null,
                        "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        postId, null, message, timestamp,
                        "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        postId, null, null, timestamp,
                        "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
                )
        );
    }

    @ParameterizedTest
    @MethodSource("arguments")
    void testSetPost(Integer id, String author, String message, Timestamp timestamp, String query) {
        when(this.dbMock.queryForObject(
                eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                any(PostDAO.PostMapper.class),
                eq(postId))
        ).thenReturn(post);

        Post p = new Post();
        p.setId(id);
        p.setAuthor(author);
        p.setMessage(message);
        p.setCreated(timestamp);

        PostDAO.setPost(postId, p);

        verify(this.dbMock).update(eq(query), (Object[]) any());
    }

    @Test
    void testSetPostWithEmpty() {
        Post p = new Post();
        PostDAO.setPost(postId, p);
        verify(this.dbMock, never()).update(Mockito.any(), (Object[]) Mockito.any());
    }
}