package com.hw.db.DAO;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class ThreadDAOTests {
    JdbcTemplate dbMock;
    ThreadDAO threadMock;

    @BeforeEach
    void init() {
        this.dbMock = Mockito.mock(JdbcTemplate.class);
        this.threadMock = new ThreadDAO(this.dbMock);
    }

    private static List<Arguments> arguments() {
        return List.of(
                Arguments.of(1, 2, 3, true,
                        "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"
                ),
                Arguments.of(1, null, 1, null,
                        "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch;"
                )
        );
    }

    @ParameterizedTest
    @MethodSource("arguments")
    void testTreeSort(Integer id, Integer limit, Integer since, Boolean desc, String query) {
        ThreadDAO.treeSort(id, limit, since, desc);
        Mockito.verify(this.dbMock).query(
                Mockito.eq(query),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.any()
        );
    }
}