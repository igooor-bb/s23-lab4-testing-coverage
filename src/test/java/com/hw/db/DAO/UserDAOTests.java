package com.hw.db.DAO;

import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Optional;
class UserDAOTests {
    JdbcTemplate dbMock;
    UserDAO userMock;

    static final String slug = "slug";
    static final String nickname = "some";

    static final String email = "email";
    static final String about = "about";

    @BeforeEach
    void setUp() {
        this.dbMock = Mockito.mock(JdbcTemplate.class);
        this.userMock = new UserDAO(this.dbMock);
    }

    private static List<Arguments> arguments() {
        return List.of(
                Arguments.of(
                        slug, email, null, null,
                        "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        slug, null, nickname, null,
                        "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        slug, null, null, about,
                        "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        slug, email, nickname, null,
                        "UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        slug, "email", null, about,
                        "UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        slug, null, nickname, about,
                        "UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        slug, "email", nickname, about,
                        "UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"
                )
        );
    }

    @ParameterizedTest
    @MethodSource("arguments")
    void testChanges(String nick, String email, String name, String info, String query) {
        UserDAO.Change(new User(nick, email, name, info));
        Mockito.verify(this.dbMock).update(Mockito.eq(query), Optional.ofNullable(Mockito.any()));
    }

    @Test
    void testDummyChanges(){
        UserDAO.Change(new User(slug, null, null, null));
        Mockito.verify(this.dbMock, Mockito.never()).update(Mockito.anyString(), Optional.ofNullable(Mockito.any()));
    }
}